#!/usr/bin/bash

mkdir ~/rakudo && cd $_
curl -O http://rakudo.org/downloads/star/rakudo-star-2016.04.tar.gz
tar -xvzf rakudo-star-2016.04.tar.gz
cd rakudo-star-2016.04/

perl Configure.pl --backend=moar --gen-moar
make

# Running the tests is optional, but advised:
make rakudo-test
make rakudo-spectest

make install

echo "export PATH=$(pwd)/install/bin/:\$PATH" >> ~/.bashrc
source ~/.bashrc
